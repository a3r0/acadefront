'use strict';

/**
 * @ngdoc function
 * @name academiaApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the academiaApp
 */
angular.module('academiaApp')
  .controller('LoginCtrl', function ($scope, api, $location, $mdDialog, user, $route) {

    $scope.form = {
      large: false
    };
    $scope.dialog = false;

    $scope.doLogin = function(form) {
      if ($scope.loading) {
        return ;
      }
      $scope.loading = true;
      $scope.error = false;
      api.$http.get('/default/login', form).then(function(res) {
        if (res && res.data && res.data.success) {
          $scope.isLogIn = true;
          user.setToken(res.data.token, !form.large);
          $location.path('/dashboard');
        } else {
          $scope.loading = false;
          $scope.error = true;
          $scope.form = {};
        }
      });
    };

    $scope.isLogIn = user.getToken();

    $scope.showDialog = function() {
      document.body.style.height = window.innerHeight + 'px';
      $mdDialog.show({
        controller: DialogController,
        templateUrl: '/views/change_course.tmpl.html',
        parent: angular.element(document.body),
        clickOutsideToClose:true,
      })
      .finally(function() {
        document.body.style.height = '100%';
      });
    };

    function DialogController($scope, $mdDialog) {
      $scope.pre = user.preferences();
      $scope.showLoader = true;
      api.$http.get('/default/curses').then(function(res) {
        $scope.showLoader = false;
        $scope.curses = res.data.curses;
      });

      $scope.changeCurse = function (id) {
        $mdDialog.hide();
        if ($scope.pre.curse != id) {
          var pre = angular.copy($scope.pre);
          pre.curse = id;
          user.preferences(pre);
          var loPath = $location.path();
          if (loPath != '/dashboard' && loPath != '/admin') {
            $location.path('/dashboard');
          }
          $route.reload();
        }
      };
    }

    $scope.$watch(function() {
      return user.getToken();
    }, function(newV) {
      $scope.isLogIn = newV;
      if ($scope.isLogIn && !user.preferences().curse) {
        $scope.showDialog();
      }

    });

    $scope.logOut = function() {
      user.clear();
      $scope.isLogIn = false;
      $location.path('/login');
    };
    $scope.dontDialog = function() {
      $scope.showDialog = console.log;
    }
  });
